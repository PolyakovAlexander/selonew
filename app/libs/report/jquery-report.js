(function ($) {
    $.fn.report = function (options) {  
        var defaults = {
            'type'           : 'success',
            'time'           : 1000,
            'animationSpeed' : 300,
            'callback'       : ''
        };
        var config = $.extend(defaults, options);

        var reportEl = $('#st-report-block');

        reportEl.removeClass();
        reportEl.addClass("block-message block-message-" + config.type);
        reportEl.children('span:last').html(config.message);

        var reportWidth = reportEl.width();

        //var _top  = $(document).scrollTop();
        var _left = $(document).scrollLeft();
        var _width = $(document).width();

        $(reportEl)
            .css({
                position : 'fixed',
                top      : 0,
                left     : _left + ((_width - reportWidth) / 2)
            })
            .click(function () {
                $(reportEl).slideUp(config.animationSpeed);
            })
        ;

        $(reportEl).slideDown(config.animationSpeed, function () {
            if (config.time > 0) {
                window.setTimeout(function () {
                    $(reportEl).slideUp(config.animationSpeed);
                }, config.time);
            }
        });

        if (config.callback.length > 0) {
            $.globalEval("$(function(){" + config.callback + "})");
        }

        window.setTimeout(function () {
            var fn = function (e) {
                $(reportEl).slideUp(config.animationSpeed, function () {
                    $(document).unbind('click', fn);
                });
            };

            $(document).bind('click', fn);
        }, 300);

        return this;
    };

    $.fn.reportTo = function (options) {
        var defaults = {
            'type' : 'true'
        };
        var config = $.extend(defaults, options);
        $.cookie('new_report_text', config.message);
        $.cookie('new_report_type', config.type);
        document.location = config.uri;

        return this;
    };

    $.fn.cabinetReport = function (options) {

        if ($('#cabinet-report-block').length == 0){
            $(document.body).append('<div class="cabinet-block-report cabinet-block-report-success" id="cabinet-report-block"><a  id="cabinet-report-block-close-link" class="cabinet-block-report-close"></a><span></span></div>');
            $('#cabinet-report-block').show(0).fadeOut(0);
        }

        var defaults = {
            'message'        : '',
            'type'           : 'success',
            'animationSpeed' : 300,
            'autoHide'       : 0,
            'button'         : false,
            'style'          : false
        };

        var config = $.extend(defaults, options);
        var reportEl = $('#cabinet-report-block');

        reportEl.width("auto");
        reportEl.removeClass();
        reportEl.addClass("cabinet-block-report cabinet-block-report-" + config.type);

        var d = $('#cabinet-report-block-close-link');
        reportEl.empty().append(d).append('<span class="text-m">' + config.message + '</span>');
        if (config.style) {
            reportEl.find('.text-m').css(config.style);
        }

        if (config.button) {
            var $elButtons = $('<div />').addClass('buttons-block');
            var $spanY = $('<span />').addClass('b-font-icon__ok');
            var $buttonY = $('<button />').addClass('yes').attr('id', 'cabinet-report-block-yes-btn').append($spanY).append(config.button);
            var $buttons = $elButtons.append($buttonY).find('button').addClass('btn-b');
            reportEl.append($elButtons);
        } else {
            reportEl.find(".buttons-block").remove();
        }

        reportEl.css("position", "absolute");
        reportEl.css("top", (($(window).height() - reportEl.outerHeight()) / 2) + $(window).scrollTop() + "px");
        reportEl.css("left", (($(window).width() - reportEl.outerWidth()) / 2) + $(window).scrollLeft() + "px");
        reportEl.fadeIn(config.animationSpeed);

        if (config.autoHide > 0) {
            reportEl.data('autoHide', setTimeout(function () {
                reportEl.fadeOut(300);
                $(document.body).off('click', fn);
                reportEl.removeData('autoHide');
            }, config.autoHide));
        }
        var fn = function (e) {
            if ($(e.target).parent('.st-tabs-single-tab').length || $(e.target).hasClass('cabinet-report-trigger')) {
                return;
            }
            if ((e.target.id !== reportEl.attr('id') && $(e.target).parents('#' + reportEl.attr('id')).length == 0)) {
                reportEl.fadeOut(300);
                $(document.body).off('click', fn);
                clearTimeout(reportEl.data('autoHide'));
                reportEl.removeData('autoHide');
            } else {
                if (e.target.id == 'cabinet-report-block-close-link' || e.target.id == 'cabinet-report-block-yes-btn') {
                    reportEl.fadeOut(300);
                    $(document.body).off('click', fn);
                    clearTimeout(reportEl.data('autoHide'));
                    reportEl.removeData('autoHide');
                    return false;
                }
            }
        };

        $(document.body).on('click', fn);
        return this;
    };

    $.fn.cabinetConfirm = function (options) {

        /**
         * true/false - еквивалент нажатия да/нет
         * @type {boolean}
         * @private
         */

        this._result = false;
        this._isOpened = false;
        var $cabinetReportBlock = $('#cabinet-report-block');

        if ($cabinetReportBlock.length == 0){
            $('body').append('<div class="cabinet-block-report cabinet-block-report-success" id="cabinet-report-block"><a  id="cabinet-report-block-close-link" class="cabinet-block-report-close"></a><span></span></div>');
            $cabinetReportBlock.show(0).fadeOut(0);
        }

        var defaults = {
            'message'        : 'Вы уверены',
            'type'           : 'confirm',
            'animationSpeed' : 300,
            'textYes'        : 'Да',
            'textNo'         : 'Нет',
            'blockOverlay'   : false,
            'requestComment' : false,
            'rqCRequire'     : false,
            'rqCPlaceholder' : '',
            'rqCMaxlength'   : 300,
            callbackFuncY    : function(){ return true; },
            callbackFuncN    : function(){ return true; }
        };

        var config = $.extend(defaults, options);
        var reportEl = $('#cabinet-report-block');
        var inst = this;
        var $elButtons = $('<div />').addClass('buttons-block');
        var $textarea = $('<textarea />').addClass('confirm-textarea').attr('placeholder', config.rqCPlaceholder).css({
            'width': '100%',
            'height': '80px',
            'margin-top': '14px'
        }).attr('maxlength', config.rqCMaxlength);
        var $elText = $('<div />').addClass('text-block').append($textarea);
        var $spanY = $('<span />').addClass('b-font-icon__ok');
        var $buttonY = $('<button />').addClass('yes').append($spanY).append(config.textYes);
        var $spanN = $('<span />').addClass('b-font-icon__cancel');
        var $buttonN = $('<button />').addClass('no').append($spanN).append(config.textNo);
        var d = $('#cabinet-report-block-close-link');
        var $buttons = $elButtons.append($buttonY).append($buttonN).find('button').addClass('btn-b');
        var $ovl;

        this.show = function() {
            inst._isOpened = true;
            inst._isCanceled = false;
            inst._textComment = '';
            reportEl.width("auto");
            reportEl.removeClass();
            reportEl.addClass("cabinet-block-report cabinet-block-report-" + config.type);
            reportEl.empty().append(d).append(config.message);
            reportEl.css("position", "absolute");
            reportEl.css("top", (($(window).height() - reportEl.outerHeight()) / 2) + $(window).scrollTop() + "px");
            reportEl.css("left", (($(window).width() - reportEl.outerWidth()) / 2) + $(window).scrollLeft() + "px");
            reportEl.fadeIn(config.animationSpeed);
            if (config.requestComment != false) {
                reportEl.append($elText);
            }
            reportEl.append($elButtons);

            $buttons.on('click', function(){
                var $butCurrent = $(this);
                inst._result = $butCurrent.hasClass('yes');
                inst._isCanceled = $butCurrent.hasClass('no');
                if (config.requestComment != false) {
                    var $elTextarea = $butCurrent.closest('.cabinet-block-report-confirm').find('.confirm-textarea');
                    inst._textComment = $elTextarea.val();
                    if (config.rqCRequire != false && inst._textComment == '' && inst._result) {
                        $elTextarea.animate({ backgroundColor: "#f22"}, 200).animate({ backgroundColor: "#fff"}, 200);
                        return false;
                    }
                }
                inst.hide();
            });

            inst._addOverley();

            $('body').off('click', this._clickFn).on('click', this._clickFn);
            $(window).mousewheel(function(event) {
                if (inst._isOpened && config.blockOverlay) {
                    event.preventDefault();
                }
            });

            $(document).keyup(function(e) {
                if (e.keyCode == 27) {
                    $buttonN.trigger('click');
                }
            });

        };

        this._addOverley = function() {
            if ($('#overlay-confirm').length == 0) {
                $ovl = $('<div />');
                $ovl.attr('id', 'overlay-confirm');
                var $body = $('body');
                $body.append($ovl);

                $body.css({
                    "overflow": 'hidden',
                    'padding-right': ($(window).width() > 480) ? '15px' : 0
                });
            } else {
                $ovl = $('#overlay-confirm')[0];
            }
        };

        this.hide = function () {
            inst._isOpened = false;
            reportEl.fadeOut(300);
            (inst._result) ? config.callbackFuncY(inst._textComment) : config.callbackFuncN(this._isCanceled);
            $($ovl).remove();
            var $body = $("body");

            $body.removeAttr('style');

            $body.off('click', this._clickFn);
        };

        this._clickFn = function (e) {
            if ((e.target.id !== reportEl.attr('id') && $(e.target).parents('#' + reportEl.attr('id')).length == 0) && !config.blockOverlay) {
                inst.hide();
            } else if (e.target.id == 'cabinet-report-block-close-link') {
                inst.hide();
                return false;
            }
        };

        this.show();
        return this;
    };

    $(function () {
        $('body').append('<div id="st-report-block" class="block-message"><span class="ico"></span><span></span></div>');
        $('#st-report-block').show(0).slideUp(0);
    });
})(jQuery);
