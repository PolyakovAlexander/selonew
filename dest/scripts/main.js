"use strict";

// DEV 
// openModal('forcibly-tour');    
var ExpressTour = new function () {
  this.step = 1;
  this.state = 'start';
  this.asideBlock = $('.aside__nav');
  this.maxSteps = this.asideBlock.find('[data-express-step]').length;
  this.popupBlock = $('.express-tour-popup');

  this.settings = function () {
    this.stepItem = this.asideBlock.find('[data-express-step=' + this.step + ']');
    this.stepParent = this.stepItem.closest('.aside__nav-item');
    this.allItemsMenu = this.asideBlock.find('.aside__nav-item');
    this.popupBlock.find('.express-tour-popup__all-steps').text(this.maxSteps);
  };

  this.changeState = function () {
    switch (this.state) {
      case 'start':
        this.popupBlock.removeClass("express-tour-popup_process");
        break;

      case 'process':
        this.popupBlock.removeClass("express-tour-popup_start");
        this.popupBlock.removeClass("express-tour-popup_finish");
        this.popupBlock.addClass("express-tour-popup_process");
        break;

      case 'finish':
        this.popupBlock.removeClass("express-tour-popup_process");
        this.popupBlock.addClass("express-tour-popup_finish");
        break;

      default:
        break;
    }
  };

  this.changeMenu = function () {
    this.allItemsMenu.removeClass('active');
    this.stepParent.addClass('active');
    this.stepItem.addClass('tour-active');
    this.popupBlock.find(".express-tour-popup__current-step").text(this.step);
    this.popupBlock.find(".express-tour-popup__step").removeClass('visible');
    this.popupBlock.find(".express-tour-popup__step[data-descr-step=" + this.step + "]").addClass('visible');
  };

  this.changeStep = function (step) {
    switch (step) {
      case 'next':
        this.step++;

        if (this.step < this.maxSteps) {
          this.state = 'process';
        } else {
          this.state = 'finish';
        }

        this.changeState();
        this.stepItem.removeClass('tour-active');
        this.settings();
        this.changeMenu();
        break;

      case 'prev':
        this.step--;

        if (this.step > 1) {
          this.state = 'process';
        } else {
          this.state = 'start';
        }

        this.changeState();
        this.stepItem.removeClass('tour-active');
        this.settings();
        this.changeMenu();
        break;

      default:
        break;
    }
  };

  this.unmount = function () {
    this.allItemsMenu.removeClass('active');
    this.stepItem.removeClass('tour-active');
  };

  this.init = function () {
    this.settings();
    this.changeMenu();
  };
}();

function checkOpeningMenu() {
  var status = localStorage.getItem('mainMenuStatus');
  console.log(status);

  if (!status) {
    localStorage.setItem('mainMenuStatus', 'closed');
  }

  if (status === 'closed' && $('.aside').hasClass('aside--collapsed')) {
    $(".header__menu").trigger('click');
  } else {}
}

checkOpeningMenu();

function openModal(name) {
  $(".modals").addClass('opened');
  $("[data-modal-name=" + name + "]").addClass('opened');

  switch (name) {
    case 'express-tour':
      var aside = $('.aside');
      if (aside.hasClass('aside--collapsed')) $(".header__menu").trigger('click');
      var asideWidth = aside.outerWidth();
      $(".modals").css('left', asideWidth);
      ExpressTour.init();
      break;

    case 'news':
      var newsCarousel = $('.js-news-carousel');
      newsCarousel.owlCarousel({
        loop: true,
        margin: 10,
        dots: true,
        items: 1
      });
      break;

    default:
      $(".modals").css({
        'left': 0,
        'top': 0
      });
      break;
  }

  $('body').addClass('ovh');
}

function closeModals() {
  if ($('.tour-active').length) {
    $(".tour-active").removeClass('tour-active');
  }

  $('.modals').removeClass('opened').css('left', 0);
  $('.modals__item').removeClass('opened');
  $('body').removeClass('ovh');
}

u('.footer-toggle__button').on('click', function () {
  u(this).toggleClass('opened');
  u(".footer").toggleClass('opened');
  var footHeight = u('.footer').size().height,
      needValue = document.body.scrollHeight - footHeight - 140;
  window.scrollTo(0, needValue);
}); /// 

u('.aside__nav-link').on('click', function () {
  if (!u(this).parent().hasClass('active')) {
    u('.aside__nav-item').removeClass('active');
  }

  u(this).parent().toggleClass('active');
});
u('.header__menu-right').on('click', function () {
  u('.header').toggleClass('header-is-right-menu');
});
u('.header__menu').on('click', function () {
  if (window.innerWidth < 1024) {
    if (window.innerWidth < 768) {
      toggleMobileAside();
    } else {
      toggleTabletAside();
    }
  } else {
    u('.aside').toggleClass('aside--collapsed');
    u('body').toggleClass('aside-is-collapsed');
  }

  setasideMenuHeight();
});
window.addEventListener('resize', function () {
  asideHandler();
});

function setasideMenuHeight() {
  var height = u('.aside').size().height - u('.aside__account').size().height - u('.aside__search').size().height - u('.aside__nav-fixed').size().height;
  u('.aside__nav-scroll-wrapper').first().style.height = height + 'px';
}

setasideMenuHeight();

function asideHandler() {
  setasideMenuHeight();

  if (window.innerWidth < 768) {
    u('.aside').removeClass('aside--collapsed');
    u('body').removeClass('aside-is-collapsed');
    return;
  }

  u('.aside__mobile-cover').remove();
  u('body').first().style.overflow = 'auto';

  if (window.innerWidth < 1024) {
    u('.aside').addClass('aside--collapsed');
    u('body').addClass('aside-is-collapsed');
  } else {
    u('.aside').removeClass('aside--collapsed');
    u('body').removeClass('aside-is-collapsed');
  }
}

asideHandler();

function toggleTabletAside() {
  if (u('.aside').hasClass('aside--collapsed')) {
    u('.aside').removeClass('aside--collapsed');
    u('body').removeClass('aside-is-collapsed');
    u('body').first().style.overflow = 'hidden';
    u('body').append('<div onclick="toggleTabletAside()" class="aside__mobile-cover"></div>');
  } else {
    u('.aside').addClass('aside--collapsed');
    u('body').addClass('aside-is-collapsed');
    u('body').first().style.overflow = 'auto';
    u('.aside__mobile-cover').remove();
  }

  setasideMenuHeight();
}

function toggleMobileAside() {
  if (u('.aside').hasClass('aside--mobile-show')) {
    u('.aside').removeClass('aside--mobile-show');
    u('body').first().style.overflow = 'auto';
    u('.aside__mobile-cover').remove();
  } else {
    u('.aside').addClass('aside--mobile-show');
    u('body').first().style.overflow = 'hidden';
    u('body').append('<div onclick="toggleMobileAside()" class="aside__mobile-cover"></div>');
  }

  setasideMenuHeight();
}

setTimeout(setasideMenuHeight, 300); //
// Modals
//

var showModalBtns = document.querySelectorAll('[data-modal]');
var modal;

if (showModalBtns.length) {
  showModalBtns.forEach(function (btn) {
    btn.addEventListener('click', showModal);
  });
}

function showModal() {
  var type = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'default';
  modal = new tingle.modal({
    closeLabel: 'esc',
    cssClass: ['tingle-modal--' + type],
    onClose: function onClose() {
      modal.destroy();
    }
  });

  if (type === 'widget') {
    var _content = document.getElementById('modal-widget').innerHTML;
    modal.setContent(_content);
    setTimeout(function () {
      new Glide('.tingle-modal--widget .glide', {
        peek: 180,
        gap: 40,
        breakpoints: {
          767: {
            peek: 80,
            gap: 20
          },
          527: {
            peek: 30,
            gap: 10
          }
        }
      }).mount();
    }, 100);
  }

  var content = document.getElementById('modal-' + type).innerHTML;
  modal.setContent(content);
  modal.open();
  tabsInit();
} //
// Tabs
//


function tabsInit() {
  tabbis.init();
}

tabsInit(); //
// Tooltips
//

var tooltipOptions = {
  animation: 'shift-toward',
  arrow: true,
  duration: [175, 150]
};
tippy.setDefaults(tooltipOptions);
u('[data-tippy-template]').each(function (elem) {
  var id = u(elem).data('tippy-template');
  var content = u('#' + id).html();
  tippy(elem, {
    content: content
  });
});